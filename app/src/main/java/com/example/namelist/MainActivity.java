package com.example.namelist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button b_add;
    EditText et_name;
    ListView lv_nameList;

    List<String> friends = new ArrayList<String>();
    String [] startingList = {"Alex", "Bart", "Carl", "Duke"};
    ArrayAdapter ad;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_add = findViewById(R.id.b_add);
        et_name = findViewById(R.id.et_name);
        lv_nameList = findViewById(R.id.lv_nameList);

        friends = new ArrayList<String>(Arrays.asList(startingList));
        ad = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, friends);
        lv_nameList.setAdapter(ad);

        b_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newName = et_name.getText().toString();
                friends.add(newName);
                ad.notifyDataSetChanged();
            }
        });

        lv_nameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, "Position = " + position + " Name = " + friends.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
